# Given an array nums containing n + 1 integers where each integer is between 1 and n (inclusive).
# Prove that at least one duplicate number must exist.
# Assume that there is only one duplicate number, find the duplicate one.

# You must not modify the array (assume the array is read only).
# You must use only constant, O(1) extra space.
# Your runtime complexity should be less than O(n2).
# There is only one duplicate number in the array, but it could be repeated more than once.


class Solution:
    def findDuplicate(self, nums):
        tortoise = nums[nums[0]]
        hare = nums[nums[nums[0]]]

        while tortoise != hare:
            tortoise = nums[tortoise]
            hare = nums[nums[hare]]

        hare = nums[0]

        while tortoise != hare:
            tortoise = nums[tortoise]
            hare = nums[hare]

        return hare
